# Lecture notes for Empirical Methods in Corporate Finance-from Professor Todd A. Gormley 

#### 介绍
来自Washington University 的 Todd A. Gormley 教授的[《公司金融中的实证研究方法》](http://www.gormley.info/phd-notes.html)讲义


#### 主要内容

1.  Introduction, Linear Regression(导论，线性回归)
2.  Linear Regression, Part 2（线性回归）
3.  Causality（因果关系）
4.  Panel Data（面板数据）
5.  Instrumental Variables（工具变量）
6.  Natural Experiment [Part 1]（自然实验）
7.  Natural Experiment [Part 2]（自然实验）
8.  Regression Discontinuity（断点回归）
9.  Common Limitations & Errors（常见限制和误区）
10. Matching & Selection Models（匹配和选择模型）
11. Standard Errors & Misc（标准误）



#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
